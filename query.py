import aiohttp
import asyncio


async def start_test(session: aiohttp.ClientSession, test_id):
    url = f'https://api.matetech.ru/api/public/companies/3/tests/{test_id}/start'
    print(url)
    async with session.post(url, json={}) as resp:
        return await resp.json()


async def init_test(session: aiohttp.ClientSession, test_attempt_id):
    url = f'https://api.matetech.ru/api/public/companies/3/test_attempts/{test_attempt_id}'
    print(url)
    async with session.get(url) as resp:
        return await resp.json()


async def get_question_info(session: aiohttp.ClientSession, test_attempt_id, question_id):
    url = f'https://api.matetech.ru/api/public/companies/3/test_attempts/{test_attempt_id}/question/{question_id}'
    print(url)
    async with session.get(url) as resp:
        return await resp.json()


async def answer_question(session: aiohttp.ClientSession, question_attempt_id, answer):
    url = f'https://api.matetech.ru/api/public/companies/3/question_attempts/{question_attempt_id}/answer'
    print(url, answer)
    async with session.post(url, json=answer) as resp:
        print(resp.status, url, answer)
        return await resp.json()


async def finish_test(session: aiohttp.ClientSession, test_attempt_id):
    url = f'https://api.matetech.ru/api/public/companies/3/test_attempts/{test_attempt_id}/finish'
    print(url)
    async with session.post(url, json={}) as resp:
        return await resp.json()


async def proccess_question(session: aiohttp.ClientSession, test_attempt_id, question_id, answer):
    question_info = await get_question_info(session, test_attempt_id, question_id)
    question_attempt_id = question_info['data']['current_attempt']['id']
    return await answer_question(session, question_attempt_id, answer)


async def proccess_test(session: aiohttp.ClientSession, test_id, answers):
    test_attempt_info = await start_test(session, test_id)
    test_attempt_id = test_attempt_info['data']['test_attempt_id']
    questions = [proccess_question(session, test_attempt_id, question_id, answer)
                 for question_id, answer in answers.items()]
    answers = await asyncio.gather(*questions)
    print(answers)
    print(await init_test(session, test_attempt_id))
    print('Ждём 5 секунд')
    await asyncio.sleep(4.5)
    print(await finish_test(session, test_attempt_id))
