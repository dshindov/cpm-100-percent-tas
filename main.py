import asyncio
import aiohttp

from analyze import get_test_data
from query import proccess_test


async def main():
    test_id, token, answers = get_test_data('phys06.har')
    headers = {'Authorization': token}
    async with aiohttp.ClientSession(headers=headers) as session:
        await proccess_test(session, test_id, answers)

if __name__ == '__main__':
    asyncio.run(main())
