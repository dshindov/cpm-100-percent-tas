import json
from haralyzer import HarParser


def get_test_data(filename='data.har'):
    with open(filename, 'r') as f:
        har_parser = HarParser(json.loads(f.read()))

    entries = list(filter(lambda x: 'api.matetech.ru' in x.request.host and x.request.accept ==
                          'application/json', har_parser.pages[0].entries))
    test_start_entries = list(
        filter(lambda x: 'start' in x.request.url, entries))
    questions_entries = list(
        filter(lambda x: '/question/' in x.request.url, entries))
    answers_entries = list(
        filter(lambda x: 'answer' in x.request.url, entries))
    test_finish_entry = list(
        filter(lambda x: 'finish' in x.request.url, entries))[0]

    assert len(test_start_entries) == 1

    print('=== Test start ===')
    print(test_start_entries[0].request.url)
    print(test_start_entries[0].request.raw_entry['postData'])
    test_id = int(test_start_entries[0].request.url[53:-6])
    token = test_start_entries[0].request.get_header_value('Authorization')
    print(json.loads(test_start_entries[0].response.text))

    questions = {}

    print('=== Questions ===')
    for entry in questions_entries:
        print(entry.request.url)
        question = json.loads(entry.response.text)['data']
        question_id = question['id']
        attempt_id = question['current_attempt']['id']
        questions[question_id] = attempt_id

    print(questions)

    attempts = {}

    print('=== Answers ===')
    for entry in answers_entries:
        print(entry.request.url)
        attempt_id = int(entry.request.url[65:-7])
        answer = json.loads(entry.request.raw_entry['postData']['text'])
        attempts[attempt_id] = answer

    print(attempts)

    print('=== Test end ===')
    print(test_finish_entry.request.url)
    print(test_finish_entry.request.raw_entry['postData'])
    print(json.loads(test_finish_entry.response.text))

    answers = {}

    for question_id, attempt_id in questions.items():
        answers[question_id] = attempts[attempt_id]

    print(answers)
    # print(token)

    # print(har_parser.pages[0].entries[3].response.text)

    return test_id, token, answers


def main():
    print(get_test_data('biol08.har'))


if __name__ == '__main__':
    main()
